class HumanPlayer
  attr_reader :name
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def display(board)
    puts board.grid
  end



  def get_move
    puts "Where would you like to move? Enter Row, then Column."
    gets.chomp.split(",").map { |x| x.to_i }
  end

end
