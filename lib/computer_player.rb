class ComputerPlayer
  attr_reader :name
  attr_accessor :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = [row, col]
        moves << pos if board.empty?(pos)
      end
    end
    moves.each do |pos|
      return pos if win?(pos)
    end
  end

  def win?(pos)
    board[pos] = mark
    if board.winner == mark
      board[pos] = nil
      true
    else
      board[pos] = nil
      false
    end
  end

end
