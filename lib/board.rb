class Board
  attr_reader :grid, :mark

  def initialize(grid = Board.default_grid)
    @grid = grid
    @mark = :X, :O
  end

  def self.default_grid
    Array.new(3) { Array.new(3) }
  end


  def place_mark(pos, mark)
    if empty?(pos)
      @grid[pos[0]][pos[1]] = mark
    else
      raise "invalid move"
    end
  end


  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

  def winner
    return :X if vertical == :X
    return :O if vertical == :O
    return :X if horizontal == :X
    return :O if horizontal == :O
    return :X if diagonal == :X
    return :O if diagonal == :O
    nil
  end

  def diagonal
    if @grid[0][0] == :X && @grid[1][1] == :X && @grid[2][2] == :X
      return :X
    elsif @grid[0][2] == :X && @grid[1][1] == :X && @grid[2][0] == :X
      return :X
    elsif @grid[0][0] == :O && @grid[1][1] == :O && @grid[2][2] == :O
      return :O
    elsif @grid[0][2] == :O && @grid[1][1] == :O && @grid[2][0] == :O
      return :O
    end
  end

  
  def over?
    return true if winner
    return true if @grid.flatten.none? { |x| x.nil? }
  end

  def horizontal
    @grid.each do |row|
      if row == [:O, :O, :O]
        return :O
      elsif row == [:X, :X, :X]
        return :X
      end
    end
  end

  def vertical
    columns = @grid.transpose
    columns.each do |col|
      if col == [:O, :O, :O]
        return :O
      elsif col == [:X, :X, :X]
        return :X
      end
    end
  end

  # def display_board(board)
  # rows = [ [], [], [] ]
  #   [0..2].each do |row|
  #     [0..2].each do |col|
  #       if board[row][col] == nil
  #         rows[row] << " "
  #       else
  #         rows[row] << board[row][col]
  #       end
  #     end
  #   end
  #   puts "|||||||"
  #   puts rows
  #   puts rows[0][0] + "|" + rows[0][1] + "|" + rows[0][2]
  #   puts "-----"
  #   puts rows[1][0] + "|" + rows[1][1] + "|" + rows[1][2]
  #   puts "-----"
  #   puts rows[2][0] + "|" + rows[2][1] + "|" + rows[2][2]
  # end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end


end
